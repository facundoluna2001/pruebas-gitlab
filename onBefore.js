module.exports = async (page, scenario, vp) => {
  const USERNAME = 'USER';
  const PASSWORD = 'PASS';

  await page.setExtraHTTPHeaders({
    Authorization: `Basic ${new Buffer(`${USERNAME}:${PASSWORD}`).toString('base64')}`
  });

  await require('./loadCookies')(page, scenario);
};
