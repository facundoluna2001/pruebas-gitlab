from pytest import raises
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
import time

def test_biogenesis(remote_browser):
    url = 'https://biodevel:b8eda32d@biodevel.wpengine.com/'
    "definimos en una variable nuestra url"
    remote_browser.get(url)
    btn_ingresar = remote_browser.find_element_by_id('menu-item-632')
    btn_ingresar.click()

    remote_browser.implicitly_wait(10)

    campo_username = remote_browser.find_element_by_id('username')
    campo_password = remote_browser.find_element_by_id('password')

    campo_username.send_keys('tester052001@gmail.com')
    campo_password.send_keys('@DK@2iRbJhNNN&eMFRdus%KL')
    # a los campos username y password le enviamos las siguientes letras
    btn_login = remote_browser.find_element_by_id('js-login-submit-btn')
    btn_login.click()
    remote_browser.implicitly_wait(8)
    producto1 = remote_browser.find_element_by_xpath(
        "//body/div[@id='page']/div[@id='content']/div[@id='primary']/main[@id='main']/section[2]/div[1]/div[2]/div[1]/div[5]/a[1]")
    producto1.click()
    remote_browser.implicitly_wait(8)
    seleccionar_cantidad = remote_browser.find_element_by_xpath('//div/input')
    seleccionar_cantidad.send_keys(1)
    btn_agregarcarrito = remote_browser.find_element_by_xpath(
        '/html[1]/body[1]/div[1]/div[1]/div[3]/div[1]/main[1]/div[2]/div[1]/div[2]/form[1]/div[1]/div[2]/div[3]/button[1]')
    btn_agregarcarrito.click()
    remote_browser.implicitly_wait(5)
    btn_agregarpedido = remote_browser.find_element_by_xpath("//a[contains(text(),'Confirmar Solicitud')]")
    btn_agregarpedido.click()

    remote_browser.implicitly_wait(5)
    remote_browser.execute_script("window.scrollTo(0,1633)")
    remote_browser.find_element(By.XPATH, "(//input[@id=\'\'])[2]").click()
    provinciaDropdown = Select(remote_browser.find_element_by_name("billing_distributor_province"))
    provinciaDropdown.select_by_visible_text("BUENOS AIRES")
    remote_browser.implicitly_wait(5)
    ciudadDropdown = Select(remote_browser.find_element_by_name("billing_distributor_locality"))
    ciudadDropdown.select_by_visible_text("LAPRIDA")
    remote_browser.implicitly_wait(5)
    veterinariaDropdown = Select(remote_browser.find_element_by_name("billing_distributor"))
    veterinariaDropdown.select_by_visible_text("BAYA CASAL SUCURSAL LAPRIDA")
    time.sleep(4)
    remote_browser.find_element(By.ID, "place_order").click()
    time.sleep(4)
    remote_browser.find_element(By.ID, "js-correct-details").click()
    remote_browser.implicitly_wait(10)
    remote_browser.find_element(By.ID,"js-keep-buying").click()
    print("La compra fue correcta")
    remote_browser.close()
